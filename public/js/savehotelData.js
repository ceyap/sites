function saveDatatoStorage() {
                
    //set by your own: total room type + room price per night + hotel building img + pax per room

    //total room types (max = 4, min = 3 per hotel)
    var total_roomtype = 4;
    var pricePnight = [100, 150, 199, 250];
    var paxProom = [2, 2, 2, 2];
    
    //pre-set arrays
    var roomIMG = [];
    var roomtype = [];

    //hotel name
    var hotelName = document.getElementById("hotelName").children[0].innerHTML;
    
    //get room img links and roomtype name
    for (var i = 1; i <= total_roomtype; i++) {

        roomIMG[i - 1] = document.getElementById("block" + i).children[0].getAttribute("src");

        roomtype[i - 1] = document.getElementById("block" + i).children[1].innerHTML; 

    }

    //save data to sessionStorage
    sessionStorage.setItem("hotelName", hotelName);
    sessionStorage.setItem("roomtype", JSON.stringify(roomtype));
    sessionStorage.setItem("pricePnight", JSON.stringify(pricePnight));
    sessionStorage.setItem("paxProom", JSON.stringify(paxProom));
    sessionStorage.setItem("roomIMG", JSON.stringify(roomIMG));
    
}