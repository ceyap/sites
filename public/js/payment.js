//-----------------------------------getdata from storage------------------------------//

var hotelName;
var roomIMG;
var roomName;
var roomQTY;
var checkInDate;
var checkOutDate;
var duration;
var roomPrice;
var num_of_rooms;
var total;
var discount = 1;


function checkChosenData() {

    if(sessionStorage.getItem("chosen_hotelName")) {

        //GET
        hotelName = JSON.parse(sessionStorage.getItem("chosen_hotelName"));
        roomIMG = JSON.parse(sessionStorage.getItem("chosen_roomIMG"));
        roomName = JSON.parse(sessionStorage.getItem("chosen_roomName"));
        roomQTY = JSON.parse(sessionStorage.getItem("chosen_roomQTY"));
        checkInDate = JSON.parse(sessionStorage.getItem("chosen_checkInDate"));
        checkOutDate = JSON.parse(sessionStorage.getItem("chosen_checkOutDate"));
        duration = JSON.parse(sessionStorage.getItem("chosen_duration"));
        roomPrice = JSON.parse(sessionStorage.getItem("chosen_roomPrice"));
        
        num_of_rooms = roomName.length;

        document.getElementById("hotelName").innerHTML = hotelName[0];

        total = 0;

        //ASSIGN 
        for (var i = 0; i < num_of_rooms; i++) {
        
            total += Number(roomPrice[i]);
            displayBookingDetails(roomIMG[i], roomName[i], roomQTY[i], 
                                checkInDate[i], checkOutDate[i], duration[i], roomPrice[i]);
            
        
            //display total with tax
            if (i == (num_of_rooms - 1)) {
        
                total *= 1.16;
                document.getElementById("total").innerHTML = total.toFixed(2);
            }
        
        }
        
        function displayBookingDetails(roomIMG, roomName, roomQTY, checkInDate, checkOutDate,
                                     duration, roomPrice) {
        
            var myBooking = document.createElement('div')
            myBooking.classList.add('my_booking')
        
            var myBookingList = document.getElementsByClassName('booking_block')[0]
        
            var writeBookingInfo = 
        
            `<div class="img_block">
                <img src="${roomIMG}" width="450px" height="270px" alt="room_image"/></div>
            </div>
        
            <div class="booking_details_block">
        
                <div class="roomCount">
                    ${roomQTY} x ${roomName}
                </div>
        
                <div class="checkin_text">
                    Check-In Date: ${checkInDate}
                </div>
        
                <div class="checkout_text">
                    Check-Out Date: ${checkOutDate}
                </div>
        
                <div class="duration_text">
                    Duration: ${duration} nights
                </div> 
        
                <div class="price_text">
                    Price: RM ${roomPrice}
                </div>
            `
            myBooking.innerHTML = writeBookingInfo
            myBookingList.append(myBooking)
        }

    } else {

        window.alert("SESSION TIMEOUT");
        window.location.href = "index.html";
    }
}



//---------------------------------main fcn-------------------------------------------//



function checkCoupon() {

    var input = document.getElementById("coupon");
    var coupon1 = "BTSH";
    var coupon2 = "GHKL";
    var coupon3 = "TSH"; 
    

    if (input.value == coupon1 && hotelName[0] == "Berjaya Times Square Hotel") {

        window.alert("VALID COUPON! YOU WILL RECEIVE 10% DISCOUNT");
        discount = 0.9;
        input.disabled = "true";
        document.getElementById("checkBTN").disabled = "true";
        document.getElementById("discount").innerHTML = " (-10%)"
        calTotalWithDiscount(discount);

    } else if (input.value == coupon2 && hotelName[0] == "Grand Hyatt Kuala Lumpur") {

        window.alert("VALID COUPON! YOU WILL RECEIVE 15% DISCOUNT");
        discount = 0.85;
        input.disabled = "true";
        document.getElementById("checkBTN").disabled = "true";
        document.getElementById("discount").innerHTML = " (-15%)"
        calTotalWithDiscount(discount);

    } else if (input.value == coupon3 && hotelName[0] == "The Saujana Hotel") {

        window.alert("VALID COUPON! YOU WILL RECEIVE 20% DISCOUNT");
        discount = 0.8;
        input.disabled = "true";
        document.getElementById("checkBTN").disabled = "true";
        document.getElementById("discount").innerHTML = " (-20%)"
        calTotalWithDiscount(discount);

    } else {

        window.alert("INVALID COUPON");
        input.value = "";
        document.getElementById("total").innerHTML = total;
    }

}

function countLetter(input) {

    document.getElementById("letter_count").innerHTML = input.value.length;
}


function calTotalWithDiscount(discount) {

    total *= discount
    document.getElementById("total").innerHTML = total.toFixed(2);

}

//change it
function saveDataToStorage() {

    var cardNum = document.getElementById("card_num").value;
    var cardName = document.getElementById("card_name").value;
    var expDate = document.getElementById("exp_date").value;
    var cvv = document.getElementById("cvv").value;
    var agree = document.getElementById("agreement").checked;
   
    
    if ((cardNum != "") && (cardName != "") && (expDate != "") && (cvv != "") && (agree)) {

        window.alert("PAYMENT SUCCESSFUL!");


        var list_booked_hotelName;
        var list_booked_roomIMG;
        var list_booked_roomName;
        var list_booked_roomQTY;
        var list_checkInDate;
        var list_checkOutDate;
        var list_duration;
        var list_total;

        if (JSON.parse(sessionStorage.getItem("list_total"))) {

            window.alert("LIST FOUND! LIST IS APPENEDED.");

            //get
            list_booked_hotelName = JSON.parse(sessionStorage.getItem("list_booked_hotelName"));
            list_booked_roomIMG = JSON.parse(sessionStorage.getItem("list_booked_roomIMG"));
            list_booked_roomName = JSON.parse(sessionStorage.getItem("list_booked_roomName"));
            list_booked_roomQTY = JSON.parse(sessionStorage.getItem("list_booked_roomQTY"));
            list_checkInDate = JSON.parse(sessionStorage.getItem("list_checkInDate"));
            list_checkOutDate = JSON.parse(sessionStorage.getItem("list_checkOutDate"));
            list_duration = JSON.parse(sessionStorage.getItem("list_duration"));
            list_total = JSON.parse(sessionStorage.getItem("list_total"));

            //assign
            for (var i = 0; i < num_of_rooms; i++) {

                list_booked_hotelName.push(hotelName[i]);
                list_booked_roomIMG.push(roomIMG[i]);
                list_booked_roomName.push(roomName[i]);
                list_booked_roomQTY.push(roomQTY[i]);
                list_checkInDate.push(checkInDate[i]);
                list_checkOutDate.push(checkOutDate[i]);
                list_duration.push(duration[i]);

                //cal the room price with tax or (if) discount
                var roomPriceWithTorD = (Number(roomPrice[i]) * discount * 1.16).toFixed(2);

                list_total.push(roomPriceWithTorD);

            }

            //save 
            sessionStorage.setItem("list_booked_hotelName", JSON.stringify(list_booked_hotelName));
            sessionStorage.setItem("list_booked_roomIMG", JSON.stringify(list_booked_roomIMG));
            sessionStorage.setItem("list_booked_roomName", JSON.stringify(list_booked_roomName));
            sessionStorage.setItem("list_booked_roomQTY", JSON.stringify(list_booked_roomQTY));
            sessionStorage.setItem("list_checkInDate", JSON.stringify(list_checkInDate));
            sessionStorage.setItem("list_checkOutDate", JSON.stringify(list_checkOutDate));
            sessionStorage.setItem("list_duration", JSON.stringify(list_duration));
            sessionStorage.setItem("list_total", JSON.stringify(list_total));

            clearChosenBuffer();

            window.location.href = "payment.html";  //payment link goes here
            
           
        } else {

            //for checking >> after need delete
            window.alert("LIST NOT FOUND! LIST IS APPENEDED.");
            

            //define
            list_booked_hotelName = [];
            list_booked_roomIMG = [];
            list_booked_roomName = [];
            list_booked_roomQTY = [];
            list_checkInDate = [];
            list_checkOutDate = [];
            list_duration = [];
            list_total = [];

            //assign

            for (var i = 0; i < num_of_rooms; i++) {

                list_booked_hotelName.push(hotelName[i]);
                list_booked_roomIMG.push(roomIMG[i]);
                list_booked_roomName.push(roomName[i]);
                list_booked_roomQTY.push(roomQTY[i]);
                list_checkInDate.push(checkInDate[i]);
                list_checkOutDate.push(checkOutDate[i]);
                list_duration.push(duration[i]);

                //cal the room price with tax or (if) discount
                var roomPriceWithTorD = (Number(roomPrice[i]) * discount * 1.16).toFixed(2);

                list_total.push(roomPriceWithTorD);

            }

            //save 
            sessionStorage.setItem("list_booked_hotelName", JSON.stringify(list_booked_hotelName));
            sessionStorage.setItem("list_booked_roomIMG", JSON.stringify(list_booked_roomIMG));
            sessionStorage.setItem("list_booked_roomName", JSON.stringify(list_booked_roomName));
            sessionStorage.setItem("list_booked_roomQTY", JSON.stringify(list_booked_roomQTY));
            sessionStorage.setItem("list_checkInDate", JSON.stringify(list_checkInDate));
            sessionStorage.setItem("list_checkOutDate", JSON.stringify(list_checkOutDate));
            sessionStorage.setItem("list_duration", JSON.stringify(list_duration));
            sessionStorage.setItem("list_total", JSON.stringify(list_total));

            clearChosenBuffer();

            window.location.href = "payment.html";  //payment link goes here

        }

    } else {

        window.alert("PLEASE FILL OUT ALL REQUIRED FIELD(S)!");

    }

}


function clearChosenBuffer() {

    sessionStorage.setItem("chosen_hotelName", "");
    sessionStorage.setItem("chosen_roomName", "");
    sessionStorage.setItem("chosen_roomIMG", "");
    sessionStorage.setItem("chosen_roomQTY", "");
    sessionStorage.setItem("chosen_checkInDate", "");
    sessionStorage.setItem("chosen_checkOutDate", "");
    sessionStorage.setItem("chosen_duration", "");
    sessionStorage.setItem("chosen_roomPrice", "");

}


//zai yan jiu zhe...
window.addEventListener("beforeunload", function askLeaveSitePermission() {

    leaveSite = confirm("LEAVE SITE? YOUR BOOKING MAY NOT BE SAVED.")

    if(leaveSite) {

        clearChosenBuffer();
        window.location.href = "booking.html";

    } 
});