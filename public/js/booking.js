 
 //---------------------------------------retriveData and allocateData section-------------------------------------//
//get data from session storage
 var hotelName = sessionStorage.getItem("hotelName");
 var roomtype = JSON.parse(sessionStorage.getItem("roomtype"));
 var pricePnight = JSON.parse(sessionStorage.getItem("pricePnight"));
 //getimage link
 var roomIMG = JSON.parse(sessionStorage.getItem("roomIMG"));
 var paxProom =  JSON.parse(sessionStorage.getItem("paxProom"));

 //assign the data to the following elements
 document.getElementById("hotelName").innerHTML = hotelName;
 var total_roomtype = roomtype.length;  //.length returns a number
 var the_4th_roomtype = document.getElementById("the_4th_roomtype");
 
function check_the_4th_roomtype ()  {

     if (total_roomtype < 4) {
         the_4th_roomtype.style.display = "none";
         
     }
 }
 
 
 for (var i = 1; i <= total_roomtype; i++) {

     document.getElementById("roomName" + i).innerHTML = roomtype[i - 1];
     document.getElementById("paxProom" + i).innerHTML = "(" + paxProom[i - 1] + " pax/room)";
     document.getElementById("pricePnight" + i).value = pricePnight[i - 1];
     document.getElementById("roomIMG" + i).src = roomIMG[i - 1];
     
 }
 //----------------------------------------main functions and calculation sections------------------------------------------//



 function refreshPage() {location.reload();}

 function calNights() {

     var checkin = document.getElementById("checkInDate");
     var checkout = document.getElementById("checkOutDate");
    
    if(checkin.value && checkout.value){
        
        var checkinDate = new Date (checkin.value);
        var checkoutDate = new Date (checkout.value);
        var today = new Date();
        var displayNights = document.getElementsByClassName("nights");
        var nightCount = Number((checkoutDate - checkinDate) / (24 * 3600 * 1000));    //for calculation in calPriceWNights()
         //change the night display
        
         for (var i = 0; i < total_roomtype; i++) {

            displayNights[i].innerHTML = parseInt((checkoutDate - checkinDate) / (24 * 3600 * 1000));
            checkin.disabled = true;
            checkout.disabled = true;
            document.getElementById("pax").disabled = true;

            //edited here
            document.getElementsByClassName("roomBox")[0].style.display = "block";
            document.getElementsByClassName("subtotal")[0].style.display = "block";

        }
        
        if (nightCount <= 0) {

            window.alert("INVALID DURATION! PLEASE TRY AGAIN!");
            refreshPage();

         } else if (nightCount > 30) {

            window.alert("MAXIMUM STAY IS 30 DAYS! PLEASE TRY AGAIN!");
            refreshPage();

        } else if ((checkinDate <= today) || (checkOutDate <= today)) {

            //edited here
             window.alert("BOOKING DATE MUST BE AT LEAST " + (today.getDate() + 1) +
                            "/" + (today.getMonth() + 1) + "/" + today.getFullYear() +  "! PLEASE TRY AGAIN!");
             refreshPage();

         } else {

            calPriceWNights(nightCount);
            
        }
    }  else {

        window.alert("PLEASE SELECT CHECK-IN AND CHECK-OUT DATES!");
        refreshPage();

    } 
}

function calPriceWNights(nightCount) {
    
    for(var i = 1; i <= total_roomtype; i++) {

        document.getElementById("priceWnight" + i).value = Number(nightCount) * 
                                                           Number(document.getElementById("pricePnight" + i).value);
    }
    
}

function giveMeThisRoom(nth_room) {  

    var qtySelector = document.getElementById("qty_room" + nth_room);              //can choose multiple roomtypes
    var usersChoice = document.getElementById("IWantThisRoom" + nth_room);
    var amount = document.getElementById("amount" + nth_room)

    //edited here
    if(qtySelector.disabled) {

         qtySelector.disabled = false;
         usersChoice.innerHTML = "Remove";

    } else {

         qtySelector.disabled = true;
         usersChoice.innerHTML = "Add";
         qtySelector.value = 0;
         amount.value = 0;
         document.getElementById("displayQTYR" + nth_room).innerHTML = qtySelector.value;
        
         calSubtotal();
        
    }
}

function calAmount(roomQty, nth_room) {

    var priceWnight = document.getElementById("priceWnight" + nth_room);
    var amount = document.getElementById("amount" + nth_room);
    
    //edited here
    if (roomQty.value > 0) {

        amount.value = Number(priceWnight.value) * Number(roomQty.value);
        document.getElementById("displayQTYR" + nth_room).innerHTML = roomQty.value;
        
        calSubtotal();
         
    } else {

        window.alert("INVALID ROOMS");
        roomQty.value = 0;
        amount.value = 0;
        document.getElementById("displayQTYR" + nth_room).innerHTML = roomQty.value;
        calSubtotal();
    }
}

function calSubtotal() {

    var amount1 = document.getElementById("amount1").value;
    var amount2 = document.getElementById("amount2").value;
    var amount3 = document.getElementById("amount3").value;
    var amount4 = document.getElementById("amount4").value;
    var subtotal = Number(amount1) + Number(amount2) + Number(amount3) + Number(amount4);
    document.getElementById("subtotal").value = subtotal;
    document.getElementById("STdisplay").innerHTML = subtotal;
}
 
 
 //---------------------------------------------------check validity before go to next page-----------------------------------------------------------//
function checkValidStay() {

    var qty_room1 = Number(document.getElementById("qty_room1").value);
    var qty_room2 = Number(document.getElementById("qty_room2").value);
    var qty_room3 = Number(document.getElementById("qty_room3").value);
    var qty_room4 = Number(document.getElementById("qty_room4").value);
    var numOfguest = Number(document.getElementById("pax").value);
    var totalRoomPAX = (paxProom[0] * qty_room1) + (paxProom[1] * qty_room2) +                   //paxProom[i] is Number datatype, so no need convert
                       (paxProom[2] * qty_room3) + (paxProom[3] * qty_room4);
    if (numOfguest > totalRoomPAX) {
        var leftoutGuest = numOfguest - totalRoomPAX;
        
        window.alert("You still have " + leftoutGuest + " guest(s) left without room!");
    } else {
        saveDataToStorage();
        
    }
}  

 //-------------------------------------------------------save data to storage----------------------------------------------------------------------------//

 //edited here

function saveDataToStorage() {

    //empty arrays

    //define arrays
    var chosen_hotelName = [];
    var chosen_roomName = [];
    var chosen_roomIMG = [];
    var chosen_roomQTY = [];
    var chosen_checkInDate = [];
    var chosen_checkOutDate = [];
    var chosen_duration = [];
    var chosen_roomPrice = [];

    //check for which chosen rooms
    for (var i = 1; i <= 4; i++) {

        if(Number(document.getElementById("qty_room" + i).value) != 0) {

            //assign
            chosen_hotelName.push(document.getElementById("hotelName").innerHTML);
            chosen_roomName.push(document.getElementById("roomName" + i).innerHTML);
            chosen_roomIMG.push(document.getElementById("roomIMG" + i).getAttribute("src"));
            chosen_roomQTY.push(document.getElementById("qty_room" + i).value);
            chosen_checkInDate.push(document.getElementById("checkInDate").value);
            chosen_checkOutDate.push(document.getElementById("checkOutDate").value);
            chosen_duration.push(document.getElementsByClassName("nights")[0].innerHTML);
            chosen_roomPrice.push(document.getElementById("amount" + i).value);

        }
    }

    var proceedToPayment = confirm("Do you wish to proceed to Payment?")

    if(proceedToPayment) {

        //save
        sessionStorage.setItem("chosen_hotelName", JSON.stringify(chosen_hotelName));
        sessionStorage.setItem("chosen_roomName", JSON.stringify(chosen_roomName));
        sessionStorage.setItem("chosen_roomIMG", JSON.stringify(chosen_roomIMG));
        sessionStorage.setItem("chosen_roomQTY", JSON.stringify(chosen_roomQTY));
        sessionStorage.setItem("chosen_checkInDate", JSON.stringify(chosen_checkInDate));
        sessionStorage.setItem("chosen_checkOutDate", JSON.stringify(chosen_checkOutDate));
        sessionStorage.setItem("chosen_duration", JSON.stringify(chosen_duration));
        sessionStorage.setItem("chosen_roomPrice", JSON.stringify(chosen_roomPrice));

        window.location.href = "payment.html";
    }
}
