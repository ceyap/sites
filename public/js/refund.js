function check(){

    if(sessionStorage.getItem("list_booked_hotelName") == null) {

        window.alert("You do not have any order")
        window.location = 'index.html';

    } else {
        getData();
    }

    var hotel = sessionStorage.getItem("list_booked_hotelName");
    var bookingAmount = (JSON.parse(hotel));
    var refunda = sessionStorage.getItem("index");
    var refund = (JSON.parse(refunda));
    if(bookingAmount.length==refund.length) {
        window.alert("You do not have any order")
        window.location = 'index.html';
    }

    if(sessionStorage.getItem("index")){
        var target = document.getElementsByClassName('room-image')
        for (var i = 0; i < refund.length; i++) {
             target[0].parentElement.remove()
        }

    }

}


//---------------------------------------------------------TAKE DATA------------------------------------------------------------------

function getData() {

var image = sessionStorage.getItem("list_booked_roomIMG");      //typeof = string
var hotel = sessionStorage.getItem("list_booked_hotelName");
var roomName = sessionStorage.getItem("list_booked_roomName");
var checkin = sessionStorage.getItem("list_checkInDate");
var checkout = sessionStorage.getItem("list_checkOutDate");
var quantity = sessionStorage.getItem("list_booked_roomQTY");
var price = sessionStorage.getItem("list_total");

ordered_roomName = (JSON.parse(roomName));
ordered_roomQTY = (JSON.parse(quantity));
ordered_hotel = (JSON.parse(hotel));
ordered_roomIMG = (JSON.parse(image));
//document.getElementById("order_price" ).innerHTML = price;
ordered_checkout = (JSON.parse(checkout));
ordered_checkin = (JSON.parse(checkin));
ordered_price = (JSON.parse(price));

for (var i = 0; i < ordered_roomName.length; i++) {
    var imagel = ordered_roomIMG[i];
    var hotell = ordered_hotel[i];
    var roomNamel = ordered_roomName[i];
    var checkinl = ordered_checkin[i];
    var checkoutl = ordered_checkout[i];
    var quantityl = ordered_roomQTY[i];
    var pricel = ordered_price[i];
    

displayOrderList(imagel, hotell, roomNamel, checkinl, checkoutl, quantityl,pricel);
}

}

function displayOrderList(imagel, hotell, roomNamel, checkinl, checkoutl, quantityl, pricel) {

    var order = document.createElement('div')
    order.classList.add('order')
    var orderList = document.getElementsByClassName('orderList')[0]
    
    var orderContents = `
    <div id="order">
    <div class="room-image">
        <img src="${imagel}" id="roomimg"  width=200px height=200px/>
    </div>
        <div class="information">
        <div>
        <span id="hotel_name">${hotell}</span>
        <p id="room_type">${roomNamel}</p>
        </div>
        <span id="checkin">${checkinl}</span>
        <span> to </span>
        <span id="checkout">${checkoutl}</span>
        </div>
    <div class="order-quantity">
        <span id="roomqty">${quantityl}</span>
    </div>
    <div >
        <span>RM </span>
        <span id="order_price">${pricel}</span>
        <button class="btn btn-refund" type="button">REFUND</button>
    </div>
    </div>`
    order.innerHTML = orderContents
    orderList.append(order)    
    order.getElementsByClassName('btn-refund')[0].addEventListener('click', refundOrder)
}



function refundOrder(event) {

    var buttonClicked = event.target
    var makes = confirm("Continue to refund this order");
    if(makes){
        buttonClicked.parentElement.parentElement.remove()
        var count = 0
        if (JSON.parse(sessionStorage.getItem("index"))) {
            index = JSON.parse(sessionStorage.getItem("index"));
            count=index[index.length-1] + 1
            index.push(count)
            sessionStorage.setItem("index", JSON.stringify(index));
        } else {
            var index = [];
            index [0] = count
            sessionStorage.setItem("index", JSON.stringify(index));
        }
    
    alert("Refund has been done")

    }
    checkorder();
}

function checkorder(){
    var hotel = sessionStorage.getItem("list_booked_hotelName");
    var bookingAmount = (JSON.parse(hotel));
    var refunda = sessionStorage.getItem("index");
    var refund = (JSON.parse(refunda));
    if(bookingAmount.length==refund.length) {
        window.alert("You do not have any order")
        window.location = 'index.html';
    }
}